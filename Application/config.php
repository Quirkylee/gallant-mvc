<?php
/**
 * Gallant MVC: Basic PHP MVC
 *     Copyright (C) 2018  ML Development
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Created by IntelliJ IDEA.
 * User: matthewl
 * Date: 11/16/18
 * Time: 11:26 AM
 */

/**
 * Sets the default controller
 */
const DEFAULT_CONTROLLER = 'Index';
/**
 * Sets the root folder of the application
 */
define('ROOT',filter_input(INPUT_SERVER, 'DOCUMENT_ROOT'));

/**
 * Database settings
 */
interface DBSettings {
    const DB_HOST = "localhost";
    const DB_USER = "";
    const DB_PASS = "";
    const DB_NAME = "";
    const DB_PREFIX = "gt_";
}
