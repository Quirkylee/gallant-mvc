<?php

/**
 * Description of Hash
 *
 * @author Quirkylee
 */
class Hash
{
    private $length;
    private $hash;

    public function generate($length = 50)
    {
        $this->length = ($length < 4) ? 4 : $length;
        $this->hash = bin2hex(random_bytes(($this->length - ($this->length % 2)) / 2));
        return $this->hash;
    }
    public function getHash() {
        return $this->hash;
    }
    public function getLength() {
        return $this->length;
    }
}
