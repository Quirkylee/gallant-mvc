<?php

class DB implements DBSettings {

    /**
     * @var Mysqli Stores the connection resource for the db;
     */
    private $conn;
    private $table;
    private $stmtprefix;
    private $fields = "";
    private $where = "";

    /**
     * Database Library constructor
     */
    public function __construct() {
        $this->conn = new mysqli(DBSettings::DB_HOST, DBSettings::DB_USER, DBSettings::DB_PASS, DBSettings::DB_NAME);
    }

    /**
     * It queries the db.
     * @param String $query
     * @return Result
     */
    public function query($query) {
        return $this->conn->query($query);
    }

    /**
     * Returns the id from last query.
     * @return int
     */
    public function insert_id() {
        return $this->conn->insert_id;
    }

    public function insert($table, $array) {
        $fields = array_keys($array);
        $values = array_values($array);
        $str = "INSERT INTO " . DBSettings::DB_PREFIX . $table . " (";
        $i = 1;
        foreach ($fields as $key) {
            if ($i !== sizeof($fields) + 1 && $i !== 1) {
                $str .= ", ";
            }
            $str .= $key;
            $i++;
        }
        $str .= ") VALUES (";
        $i = 1;
        foreach ($values as $value) {
            if ($i !== sizeof($values) + 1 && $i !== 1) {
                $str .= ", ";
            }
            $str .= "'" . $value . "'";
            $i++;
        }
        $str .= ");";
        //echo "<code>".$str."</code>";
        $this->query($str);
    }

    public function select($fields) {
        $this->stmtprefix = "SELECT " . $fields . " FROM ";
        return $this;
    }

    public function from($table) {
        $this->table = DBSettings::DB_PREFIX . $table;
        return $this;
    }

    /**
     * updates values of records
     * @param string $table
     * @param array $data
     */
    public function update($table, array $data) {
        $this->table = DBSettings::DB_PREFIX . $table;
        $this->stmtprefix = "UPDATE ";
        $this->fields = " SET ";
        if (!is_string($data)) {
            $index = 1;
            foreach ($data as $key => $value) {
                $this->fields .= $key . " = '" . $value . "'";
                if ($index < count($data)) {
                    $this->fields .= ", ";
                }
                $index++;
            }
        } else {
            $this->fields .= $data;
        }
        return $this;
    }

    public function where($data, $oprand = NULL) {
        $this->where = " WHERE";
        if (!is_string($data)) {
            for ($index = 0; $index < count($data); $index++) {
                $key = key($data[$index]);
                $value = $data[$index];
                $this->where .= " ".$key . " = '" . $value . "'";

                if ($index !== count($data)) {
                    $this->oprand($oprand);
                }
            }
        } else {
            $this->where .= " ".$data;
        }
        return $this;
    }

    public function oprand($oprand = NULL) {
        if ($oprand !== NULL) {
            $this->where = " " . $oprand;
        } else {
            $this->where = " AND";
        }
        return $this;
    }
    public function exec() {
        $statement = $this->stmtprefix.$this->table. $this->fields.$this->where;
        return $this->query($statement);
    }
    public function affected_rows() {
        return $this->conn->affected_rows;
    }
    public function delete() {
        $this->stmtprefix = "DELETE FROM ";
        return $this;
    }
    /**
     * Escapes string for db.
     * 
     * @param string $string
     * @return string
     */
    public function escape_string(string $string) {
        return $this->conn->real_escape_string($string);
    }
}

$db = new DB();

