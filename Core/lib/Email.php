<?php

/**
 * Email handling library
 *
 * @author Quirkylee
 */
class Email {

    private $headers = NULL;
    private $metaData;
    private $body;
    private $to;
    private $subject;
    private $boundary;
    private $text;
    private $html;
    private $mime;

    public function __construct(string $to = NULL) {

        $this->to = $to;
        $this->boundary = uniqid('jo');
    }
    public function to(string $to) {
        $this->to = $to;
    }
    public function headers($headers) {
        $this->headers = "";
        if (is_array($headers)) {
            foreach ($headers as $key => $value) {
                $this->headers .= $key . ": " . $value . "\r\n";
            }
        } else {
            $this->headers = $headers;
        }
        return $this;
    }

    public function metaData(array $array) {
        $this->metaData = $array;
        return $this;
    }

    public function body(string $str) {
        $this->body = $this->body = $str;
        return $this;
    }

    public function subject(string $subject) {
        $this->subject = $subject;
        return $this;
    }
    public function text(string $text) {
        $this->text = $text;
        return $this;
    }
    public function html(string $html) {
        $this->html = $html;
        return $this;
    }

    public function send() {
        if (!isset($this->headers)) {
            $this->headers = "From: ML Development <noreply@mldevelopment.pro>\r\n";
            $this->headers .= "MIME-Version: 1.0\r\n";
            if (isset($this->text) && isset($this->html)) {
                $this->mime = "multipart";
                $this->headers .= "Content-Type: multipart/alternative; boundary=\"" . $this->boundary . "\"\r\n";
            } elseif (isset($this->text)) {
                $this->headers .= "Content-Type: text/plain; boundary=\"" . $this->boundary . "\"\r\n";
                $this->mime = "text";
            } elseif (isset($this->html)) {
                $this->mime = "html";
                $this->headers .= "Content-Type: text/html; boundary=\"" . $this->boundary . "\"\r\n";
            }
        }
        
        if (isset($this->text)) {
            if($this->mime == "multipart") {
                    $this->body .= "--".$this->boundary."\r\n";
                    $this->body .= "Content-Type: text/plain;\r\n\r\n";
            }
            $this->body .= $this->text;
        }
        if (isset($this->html)) {
            if($this->mime == "multipart") {
                    $this->body .= "\r\n--".$this->boundary."\r\n";
                    $this->body .= "Content-Type: text/html;\r\n\r\n";
            }
            $this->body .= $this->html;
        }
        if($this->mime == "multipart") {
                    $this->body .= "\r\n--".$this->boundary."--\r\n";
            }
        if (isset($this->metaData)) {

            foreach ($this->metaData as $key => $value) {
                $this->body = str_replace($key, $value, $this->body);
            }
        }
        mail($this->to, $this->subject, $this->body, $this->headers);
    }

}
