<?php
/**
 * Gallant MVC: Basic PHP MVC
 *     Copyright (C) 2018  ML Development
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Created by IntelliJ IDEA.
 * User: matthewl
 * Date: 11/16/18
 * Time: 10:58 AM
 */
class Controller
{
    public $view;
    private $models = array();
    public $email;
    public $hash;
    public $security;
    /**
     * @var string Stores the configs file location.
     */
    public $Config_Location;

    public function __construct($get)
    {
        $array = array(
            'Core/Model.php',
            'Core/View.php',
            'Core/lib/Email.php',
            'Core/lib/Hash.php',
            'Core/lib/Security.php');
        $this->require_file($array);
        $this->view = new View();
        $this->email = new Email();
        $this->hash = new Hash();
        $this->security = new Security();

        if (isset($get[1]) && $get[1] !== '') {
            $m = $get[1];
        } else {
            $m = 'init';
        }

        if(isset($get[3])) {
            $state = TRUE;
            $data = array();
            $key = NULL;
            $count = count($get);

            for ($i = 2; $i < $count; $i++) {
                if($state) {
                    $key = $get[$i];
                    $state = FALSE;
                } else {
                    $data[$key] = $get[$i];
                    $state = TRUE;
                }
            }
            $this->$m($data);
        } else {
            $this->$m();
        }

    }

    /**
     * Allows you to require file(s) at controller level
     * @param mixed $files
     */
    public function require_file($files)
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                require_once $file;
            }
        } else {
            require_once $files;
        }

    }

    /**
     * Allows you to include file(s) at controller level
     * @param mixed $files
     */
    public function include_file($files)
    {
        if (is_array($files)) {
            foreach ($files as $file) {
                include_once $file;
            }
        } else {
            include_once $files;
        }
    }

    /**
     * loads a modal into memory
     * @param $modal
     */
    public function model($modal)
    {
        require_once ROOT.'/Application/Modal/' . $modal . ".php";
        $modal = $modal;
        if (!isset($this->models[$modal])) {
            $this->models[$modal] = new $modal();

        }
        return $this->models[$modal];

    }

    /**
     * allows you to load a class containing snipits of code to be reused on views
     * @param $class
     * @return mixed
     */
    public function snipits($class) {
        $this->require_file(ROOT."/Application/Snipits/".$class.".php");
        return new $class();
    }
}