<?php
/**
 * Gallant MVC: Basic PHP MVC
 *     Copyright (C) 2018  ML Development
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Created by IntelliJ IDEA.
 * User: matthewl
 * Date: 11/16/18
 * Time: 10:28 AM
 */
class Gallant
{
    public $controller;

    public function __construct($Config = "Application/config.php")
    {
        $get = filter_input_array(INPUT_GET);
        require_once $Config;
        if (isset($get['c'])) {
            $uri = explode("/", $get['c']);
            $this->controller = $uri[0];
        } else {
            $uri = '';
            $this->controller = DEFAULT_CONTROLLER ? DEFAULT_CONTROLLER : "Index";
        }
        require_once 'Application/Controller/' . $this->controller . '.php';
        $this->controller = new $this->controller($uri);
        $this->controller->Config_Location = $Config;


    }


}